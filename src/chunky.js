window.addEventListener("load", function(e) {
	let nav = document.querySelector(".nav");
	if(nav) {
		let pages = {
			"/": "News",
			"/films/": "Films",
			"/projects/": "Projects",
			"/bio/": "About Us",
			"/contact/": "Contact"
		};
		let current = null;
		for(path in pages) {
			if(document.location.toString().indexOf(path) >= 0) {
				current = path;
			}
		}
		let ul = document.createElement("ul");
		let li = null;
		let a = null;
		for(path in pages) {
			li = document.createElement("li");
			if(current === path) {
				li.className = "current";
			}
			a = document.createElement("a");
			a.href = path;
			a.textContent = pages[path];
			li.appendChild(a);
			ul.appendChild(li);
		}
		nav.appendChild(ul);
	}
});

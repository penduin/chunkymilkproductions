"use strict";
var STD = {
  W: 128,
  H: 128,
  scale: null,
  canvas: null,
  ctx: null,
  gpx: null,
  music: null,
  musicended: false,
  sfx: {
    gong: null,
    fire: null
  },
  progress: 0,
  survived: 0,
  control: [],   // 0, 1, ... indexes of controllable dancers
  ui: {
    action: null,
    left: null,
    right: null
  },
  input: {
	left: false,
	right: false,
	action: false,
	pointer: {
	  x: 0,
	  y: 0,
	  click: false
	}
  },
  dancers: [],   // { who: 0-4, x, y, speedx, speedy, toffset }
  NUM_DANCERS: 4,
  MAX_SPEED: 3,
  flames: [],    // { x, y, speed, shape, scale }
  wind: {
    speed: 0,    // left=-, right=+
    influence: 0,// +/- small amt each frame, influence wind over time
    fx: []       // { x, y, shape }
  },
  MAX_WIND: 1.5,   // (absolute) wind max
  fall: [],      // { x, y, speed, shape, scale, bonked }
  TO_RADIANS: Math.PI / 180,
  mode: "title",
  nextmode: "",
  modestart: 0,
  modeend: 0,
  menu: null,
  ready: false,
  lastframe: 0,
  frametime: 0,
  pause: 0,
  tickwait: Math.floor(1000 / 60), //60fps
  tick: 0,
  touch: 0
};

function localGet(key) {
	if(localStorage) {
		return localStorage.getItem(key);
	}
}
function localSet(key, val) {
	if(localStorage) {
		return localStorage.setItem(key, val);
	}
}

function lerp(from, to, prog) {
	return (1 - prog) * from + prog * to;
};
function derp(from, to, prog) {
	// digital lerp.  get it?  ...derp.
	return Math.floor(lerp(from, to, prog));
};

function blink() {
	return Math.floor(STD.tick / 10) % 2;
}

function randomdirection() {
	return "nesw".charAt(Math.floor(Math.random() * 4));
}

function draw(p, x, y) {
	var scale = STD.scale;
	var i, j;
	var ctx;
	if(typeof x === undefined) {
		x = p.x || 0;
	}
	if(typeof y === undefined) {
		y = p.y || 0;
	}
	STD.ctx.save();
	if(!p.canvas) {
		p.canvas = document.createElement("canvas");
		p.canvas.width = p.canvas.height = 8;
		ctx = p.canvas.getContext("2d");
		ctx.fillStyle = [
			"rgb(",
			Math.floor(p.color.r * 255), ",",
			Math.floor(p.color.g * 255), ",",
			Math.floor(p.color.b * 255),
			")"
		].join("");
		p.shape.every(function(slice, i) {
			for(j = 0; j < 8; ++j) {
				if(slice & Math.pow(2, j)) {
					ctx.fillRect(i, j, 1, 1);
					ctx.fillRect(7 - i, j, 1, 1);
				}
			}
			return true;
		});
	}
	if(!p.canvaswalk) {
		p.canvaswalk = document.createElement("canvas");
		p.canvaswalk.width = p.canvaswalk.height = 8;
		ctx = p.canvaswalk.getContext("2d");
		ctx.drawImage(p.canvas, 0, 0, 8, 6,
					  0, 0, 8, 6);
		ctx.drawImage(p.canvas, 0, 6, 4, 2,
					  0, 6, 4, 2);
		ctx.drawImage(p.canvas, 4, 6, 4, 2,
					  4, 5, 4, 2);
	}

	if(p.crown) {
		STD.ctx.drawImage(STD.gpx,
						  blink() ? 8 : 0,
						  8 * 3, 8, 8,
						  x * scale, (y - 8) * scale,
						  8 * scale, 8 * scale);
	}

	if(p.walking) {
		STD.ctx.translate(x * scale, y * scale);
		if(blink()) {
			STD.ctx.scale(-1, 1);
			STD.ctx.translate(-8 * scale, 0);
		}
		STD.ctx.drawImage(p.canvaswalk,
						  0, 0, 8, 8,
						  0, 0,
						  8 * scale, 8 * scale);
	} else {
		STD.ctx.drawImage(p.canvas, 0, 0, 8, 8,
						  x * scale, y * scale, 8 * scale, 8 * scale);
	}

	STD.ctx.restore();
}

function text(str, x, y, scale) {
	scale = scale ? STD.scale * scale : STD.scale;
	var c;
	STD.ctx.save();
	for(var i = 0; i < str.length; ++i) {
		c = str.charCodeAt(i);
		STD.ctx.drawImage(STD.gpx, c % 32 * 8,
						  Math.floor((c-32) / 32) * 8,
						  8, 8, x * scale, y * scale, 8 * scale, 8 * scale);
		x += 8;
	}
	STD.ctx.restore();
}

function dancer_name(who) {
  switch(who) {
  case 0:
    return "Screamy";
    break;
  case 1:
    return "Nu-Mi";
    break;
  case 2:
    return "Yellow Goo";
    break;
  case 3:
    return "Terry";
    break;
  default:
    return "Everybody!";
    break;
  }
}
function dancer_part(who) {
  switch(who) {
  case 0:
    return "Bass";
    break;
  case 1:
    return "Melody";
    break;
  case 2:
    return "Under-Melody";
    break;
  case 3:
    return "Barritone";
    break;
  default:
    return "All Parts!";
    break;
  }
}

function song_loaded(e) {
  STD.music.play();
}
function song_ended(e) {
  STD.musicended = true;
  STD.music.removeEventListener("ended", song_ended);
  STD.music.removeEventListener("canplaythrough", song_loaded);
  STD.music = null;
}
function start_song(which) {
  if(STD.music) {
    STD.music.pause();
    STD.music.currentTime = 0;
    STD.music.removeEventListener("ended", song_ended);
  } else {
    STD.music = document.createElement("audio");
    document.body.appendChild(STD.music);
  }
  switch(which) {
    /*
  case 0:
    STD.music = document.querySelector("audio#song4");
    break;
  case 1:
    STD.music = document.querySelector("audio#song1");
    break;
  case 2:
    STD.music = document.querySelector("audio#song2");
    break;
  case 3:
    STD.music = document.querySelector("audio#song3");
    break;
  default:
    STD.music = document.querySelector("audio#songa");
    break;
    */
  case 0:
    STD.music.src = "song-4.mp3";
    break;
  case 1:
    STD.music.src = "song-1.mp3";
    break;
  case 2:
    STD.music.src = "song-2.mp3";
    break;
  case 3:
    STD.music.src = "song-3.mp3";
    break;
  default:
    STD.music.src = "song-a.mp3";
    break;
  }
  STD.music.addEventListener("ended", song_ended);
  STD.music.volume = 1.0;
//  STD.music.controls = 1;
//  STD.music.style.position = "absolute";
//  STD.music.style.bottom = "0";
//  STD.music.style.left = "0";
  STD.music.addEventListener("canplaythrough", song_loaded); //stupid apple
  STD.music.play();
}

function init() {
  STD.dancers = [];
  for(var i = 0; i < STD.NUM_DANCERS; ++i) {
    STD.dancers.push({
      who: i,
      x: Math.random() * STD.W,
      y: Math.random() * STD.H,
      speedx: Math.random() * STD.MAX_SPEED,
      speedy: Math.random() * STD.MAX_SPEED,
/*
      x: 0,
      y: i * 20 + 16,
      speedx: STD.MAX_SPEED / (i+1),
      speedy: 0,
*/
      toffset: Math.random() * 150
    });
  }
  STD.modeend = 0;
  STD.ctx.canvas.width = STD.ctx.canvas.width;
  STD.ctx = STD.canvas.getContext("2d");
  STD.ctx.imageSmoothingEnabled = false;
  STD.ctx.mozImageSmoothingEnabled = false;
  STD.ctx.webkitImageSmoothingEnabled = false;
  STD.modestart = 0;

  STD.mode = "title";
  STD.ready = true;
}

function draw_gpx(x, y, scale, flip) {
  STD.ctx.save();
//  STD.ctx.translate(Math.floor(x * STD.scale), Math.floor(y * STD.scale));
  if(flip) {
    STD.ctx.scale(-1, 1);
  }
  if(scale) {
    STD.ctx.scale(scale, scale);
  }
  STD.ctx.drawImage(STD.gpx, 16 * x, 16 * y,
                    16, 16,
                    -8 * STD.scale, -8 * STD.scale,
                    16 * STD.scale, 16 * STD.scale);
  STD.ctx.restore();
}
function draw_sios(who, x, y, flip) {
  STD.ctx.save();
  STD.ctx.translate(Math.floor(x * STD.scale), Math.floor(y * STD.scale));
  if(flip) {
    STD.ctx.scale(-1, 1);
  }
  STD.ctx.drawImage(STD.gpx, 16 * who, 32,
                    16, 16,
                    -8 * STD.scale, -8 * STD.scale,
                    16 * STD.scale, 16 * STD.scale);
  STD.ctx.restore();
}
function draw_dancer(dancer, time) {
  draw_sios(dancer.who, dancer.x, dancer.y,
            Math.floor((time / 150) + dancer.toffset) % 2);
}

function render_title(time) {
//  time -= STD.modestart; // relative time
  STD.ctx.save();
  STD.ctx.fillStyle = "teal";
  STD.ctx.fillRect(0, 0,
				   STD.canvas.width,
				   STD.canvas.height);
  var scale = 1;
  STD.ctx.save();

  STD.ctx.translate(64 * STD.scale, 64 * STD.scale);
  if(time - STD.modestart < 2000) {
    if(time - STD.modestart < 500) {
      STD.ctx.scale(0, 0);
    } else {
      STD.ctx.scale(((time-STD.modestart)-500) / 1500,
                    ((time-STD.modestart)-500) / 1500);
      STD.ctx.rotate((time-STD.modestart) / 500 * 360 * STD.TO_RADIANS);
    }
  }
  STD.ctx.drawImage(STD.gpx, 128, 32,
					128, 128,
					Math.floor(-64 * STD.scale), Math.floor(-64 * STD.scale),
					128 * STD.scale, 128 * STD.scale);
  //text("SEe THEm DANCe", 2 * 8, 2 * 8, 2);
  STD.ctx.restore();

  STD.dancers.forEach(function(dancer) {
    draw_dancer(dancer, time);
  });

  if(STD.modestart && time - STD.modestart < 1000) {
    //fade in
    STD.ctx.fillStyle = "black";
    STD.ctx.globalAlpha = (1000 - (time - STD.modestart)) / 1000;
    STD.ctx.fillRect(0, 0,
				     STD.canvas.width,
				     STD.canvas.height);
  }
  STD.ctx.restore();
}

function render_select(time) {
  STD.ctx.save();
  STD.ctx.fillStyle = "purple";
  STD.ctx.fillRect(0, 0,
				   STD.canvas.width,
				   STD.canvas.height);
//  text("This time,", 8*1, 8*1);
  text("You are:", 8*1, 8*2);

  var sel = 0;
  if(STD.menu === null) {
    sel = Math.floor((time / 100)) % (STD.NUM_DANCERS + 1);
  } else {
    sel = STD.menu;
  }
  STD.ctx.fillStyle = "limegreen";
  text(dancer_name(sel), 8*3, 8*8);

  var drawselect = true;
  if(STD.ready) {
  } else {
    text("Singing:", 8*1, 8*11);
    text(dancer_part(sel), 8*3, 8*12);
    drawselect = blink();
  }
  if(drawselect) {
    if(sel === STD.NUM_DANCERS) {
      STD.ctx.fillRect(0, 32 * STD.scale,
				       STD.canvas.width, 32 * STD.scale);
    } else {
      STD.ctx.fillRect((sel * 32 * STD.scale), 32 * STD.scale,
				       32 * STD.scale, 32 * STD.scale);
    }
  }

  STD.dancers.forEach(function(dancer) {
    if(!STD.ready && dancer.who !== sel && sel !== STD.NUM_DANCERS) {
      return;
    }
    draw_dancer(dancer, time);
  });

  STD.ctx.restore();
}

function render_play(time) {
  var cur = (STD.music ? STD.music.currentTime : 21.8335);
//  var len = STD.music.duration;
  var len = 21.8335; //stupid apple
  STD.ctx.save();

  //fade to night
  var bg = [
    "rgb(",
    derp(0x99, 0x00, cur/len), ",",
    derp(0xaa, 0x11, cur/len), ",",
    derp(0xff, 0x33, cur/len), ")"
  ].join("");
  STD.ctx.fillStyle = bg;

  STD.ctx.fillRect(0, 0,
				   STD.canvas.width,
				   STD.canvas.height);
//  text(bg, 0, 0);
//  text(STD.music.currentTime.toString(), 0, 0);

  if(cur < 3.1) {
    if(cur > 0.8) {
      text("1...", 8*2, 8*7);
    }
    if(cur > 1.5) {
      text("2...", 8*6, 8*7);
    }
    if(cur > 2.2) {
      text("3...", 8*10, 8*7);
    }
    text("Tap the ARROWS", 8*1, 8*14);
    text("|            |", 8*1, 8*15);
  } else {
    var lyrics = [
      { time: 0, line: "SEE THEM DANCE" },
      { time: 0, line: "SEE THEM DANCE" },
      { time: 0, line: "ON THE WIND" },
      { time: 0, line: "IN THE NIGHT" },
      { time: 0, line: "SEE THEM FALL" },
      { time: 0, line: "SEE THEM FALL" },
      { time: 0, line: "ON THE FIRE" },
      { time: 0, line: "OH SO BRIGHT" },
      { time: 0.25, line: "SO BRIGHT" },
      { time: 0.75, line: "OOOH" },
    ];
    var start = 3.1;
    var step = 1.55;
    for(var i = 0; i < lyrics.length; ++i) {
      if(cur > start + (i * step) + lyrics[i].time) {
        text(lyrics[i].line, (8*8)-(8*lyrics[i].line.length/2), 8*(3 + i));
      }
    }
  }

  STD.dancers.forEach(function(dancer) {
    if(STD.control.indexOf(dancer.who) >= 0) {
      draw_dancer(dancer, time);
    }
  });

  STD.wind.fx.forEach(function(fx) {
    // flicker, more gone == slower wind
    if(Math.abs(STD.wind.speed) < Math.random()) return;

    STD.ctx.save();
    STD.ctx.translate(Math.floor(fx.x * STD.scale),
                      Math.floor(fx.y * STD.scale));
    draw_gpx(fx.shape, 4, 1, STD.wind.speed < 0);
    STD.ctx.restore();
  });

  STD.fall.forEach(function(fall) {
    STD.ctx.save();
    STD.ctx.translate(Math.floor(fall.x * STD.scale),
                      Math.floor(fall.y * STD.scale));
    draw_gpx(fall.shape, 3, fall.scale, fall.flip);
    STD.ctx.restore();
  });

  STD.flames.forEach(function(flame) {
    STD.ctx.save();
    STD.ctx.translate(Math.floor(flame.x * STD.scale),
                      Math.floor(flame.y * STD.scale));
    if(Math.random() < 0.3) {
      STD.ctx.globalCompositeOperation = "lighter";
    }
    draw_gpx(flame.shape, 5, flame.scale, Math.random() < 0.5);
    STD.ctx.restore();
  });

  STD.ctx.restore();
}

function render_finish(time) {
  STD.ctx.save();
  var win = STD.progress === 1;
  if(win) {
    //STD.ctx.fillStyle = "dodgerblue";
    STD.ctx.fillStyle = "limegreen";
  } else {
    STD.ctx.fillStyle = "brown";
  }
  STD.ctx.fillRect(0, 0,
				   STD.canvas.width,
				   STD.canvas.height);

  STD.dancers.forEach(function(dancer) {
    if(STD.control.indexOf(dancer.who) >= 0) {
      draw_dancer(dancer, time);
    }
  });

  if(win) {
    text("WINNER!", 8*0.5, 8*0.5, 2);
    text("Hew yeh!", 8*4, 8*3);
  } else {
    text("GONG!", 8*1.5, 8*1, 2);
  }
  text("Progress:", 8*1, 8*6);
//  text(STD.progress.toString(), 0, 0);

  STD.ctx.drawImage(STD.gpx, 0, 16 * 6, 16 * 6, 16 * 1,
                    8 * 2 * STD.scale, 8 * 7 * STD.scale,
                    16 * 6 * STD.scale, 16 * 1 * STD.scale);

  var w = derp(0, (16 * 6) - 4, Math.min((time - STD.modestart) / 2000,
                                         STD.progress));
  STD.ctx.drawImage(STD.gpx, 0, 16 * 7, w, 16 * 1,
                    8 * 2 * STD.scale, 8 * 7 * STD.scale,
                    w * STD.scale, 16 * 1 * STD.scale);
  text(Math.floor(Math.min(w / (16 * 6 - 4), STD.progress) * 100) + "%",
       8*10, 8*6);
  if(Math.floor(Math.min(w / (16 * 6 - 4), STD.progress) * 100) === 69 &&
     time - STD.modestart > 2000) {
    text("(NICE)", 8*10, 8*9);
  }

  text("Survived:" + STD.survived, 8*1, 8*11);
  text("rounds", 8*10, 8*12);

  if(STD.modeend && STD.modeend - time < 1000) {
    //fade out
    STD.ctx.fillStyle = "black";
    STD.ctx.globalAlpha = (500 - (STD.modeend - time)) / 500;
    STD.ctx.fillRect(0, 0,
				     STD.canvas.width,
				     STD.canvas.height);
  }
  STD.ctx.restore();
}

function touching(npcs) {
	var touch = null;
	var x = STD.player.x;
	var y = STD.player.y;
	(npcs || []).every(function(p) {
//		touch = p;
		if(Math.max(x, p.x) - Math.min(x, p.x) <= 8 &&
		   Math.max(y, p.y) - Math.min(y, p.y) <= 8) {
			touch = p;
		}
		return !touch;
	});
	return touch;
}

function bounce_dancers() {
  var i;
  var sorted = [];
  for(i = 0; i < STD.dancers.length; ++i) {
    sorted[i] = STD.dancers[i];
  }
  sorted = sorted.sort(function(a, b) {
    return a.x - b.x;
  });
  sorted = sorted.sort(function(a, b) {
    return a.y - b.y;
  });
  for(i = 1; i < sorted.length; ++i) {
    if(Math.abs(sorted[i].x - sorted[i - 1].x) < 12 &&
       Math.abs(sorted[i].y - sorted[i - 1].y) < 12) {
      //console.log("contact", sorted[i].who, sorted[i - 1].who);
      /*
      sorted[i - 1].speedy *= -1;
      sorted[i].speedy *= -1;
      sorted[i - 1].speedx *= -1;
      sorted[i].speedx *= -1;
      */
      //bounce y speed according to how close x positions are
      sorted[i - 1].speedy *= (-1 *
                               ((16- Math.abs(sorted[i].x-sorted[i-1].x)) /16));
      sorted[i].speedy *= (-1 *
                           ((16- Math.abs(sorted[i].x - sorted[i - 1].x)) /16));
      //and vice-versa
      sorted[i - 1].speedx *= (-1 *
                               ((16- Math.abs(sorted[i].y-sorted[i-1].y)) /16));
      sorted[i].speedx *= (-1 *
                           ((16 - Math.abs(sorted[i].y - sorted[i-1].y)) / 16));

      //break 'em up
      if(Math.abs(sorted[i].x - sorted[i - 1].x) <
         Math.abs(sorted[i].y - sorted[i - 1].y)) {
        //push away in y direction
        if(sorted[i].y < sorted[i - 1].y) {
          sorted[i].y -= 1;
          sorted[i-1].y += 1;
        } else {
          sorted[i].y += 1;
          sorted[i-1].y -= 1;
        }
      } else {
        //push away in x direction
        if(sorted[i].x < sorted[i - 1].x) {
          sorted[i].x -= 1;
          sorted[i-1].x += 1;
        } else {
          sorted[i].x += 1;
          sorted[i-1].x -= 1;
        }
      }
      
      /* dangerous
      while(Math.abs(sorted[i].x - sorted[i - 1].x) < 12 &&
            Math.abs(sorted[i].y - sorted[i - 1].y) < 12) {
        sorted[i].x += sorted[i].speedx;
        sorted[i].y += sorted[i].speedy;
        sorted[i-1].x += sorted[i-1].speedx;
        sorted[i-1].y += sorted[i-1].speedy;
      }
        */
    }
  }
}

function bonk_fallers() {
  STD.dancers.forEach(function(dancer) {
    if(STD.control.indexOf(dancer.who) >= 0) {
      // we control this dancer
      STD.fall.forEach(function(fall) {
        if(Math.abs(dancer.x - fall.x) < 12 &&
           dancer.y - fall.y < 12 && dancer.y - fall.y > 0) {
          STD.sfx.bonk.currentTime = 0;
          STD.sfx.bonk.play();
          dancer.speedy = Math.max(fall.speedy, dancer.speedy + fall.speedy);
          fall.speedy = 0.2;
        }
      });
    }
  });
}

function gravity(object) {
  if(-0.05 < object.speedy && object.speedy < 0.05) {
    object.speedy += 0.1;
  }
  if(object.speedy > 0) {
    object.speedy /= 0.9;
    object.speedy = Math.min(STD.MAX_SPEED, object.speedy);
  } else {
    object.speedy *= 0.9;
    object.speedy = Math.max(0 - STD.MAX_SPEED, object.speedy);
  }
}

function move_dancer(dancer) {
  gravity(dancer);

  //airbrake
  dancer.speedx *= 0.98;

  //bounce off other dancers
  bounce_dancers();

  //move
  dancer.x += dancer.speedx;
  dancer.y += dancer.speedy;

  //edge bounce (and shove down)
  //TODO: if later stages, die?
  if(dancer.x > STD.W) {
    dancer.x = STD.W;
    dancer.speedx *= -1;
    dancer.speedy += 2;
  } else if(dancer.x < 0) {
    dancer.x = 0;
    dancer.speedx *= -1;
    dancer.speedy += 2;
  }
  if(dancer.y > STD.H) {
    dancer.y = STD.H;
    dancer.speedy *= -1;
  } else if(dancer.y < 0) {
    dancer.y = 0;
    dancer.speedy *= -1;
  }


  //TODO: other-body bounce
}
function upleft_dancer(dancer) {
  dancer.speedx -= 1.5;
  dancer.speedx = Math.max(0 - STD.MAX_SPEED, dancer.speedx);
  dancer.speedy -= 3;
  dancer.speedy = Math.max(0 - STD.MAX_SPEED, dancer.speedy);
}
function upright_dancer(dancer) {
  dancer.speedx += 1.5;
  dancer.speedx = Math.min(STD.MAX_SPEED, dancer.speedx);
  dancer.speedy -= 3;
  dancer.speedy = Math.max(0 - STD.MAX_SPEED, dancer.speedy);
}

function tick(time) {
  var row = 0;
  var col = 0;
  var i = 0;

  if(!STD.modestart) {
    STD.modestart = time;
  }

  switch(STD.mode) {
  case "title":
  default:
    STD.dancers.forEach(move_dancer);
    if(Math.random() < 0.05) {
      upleft_dancer(STD.dancers[Math.floor(Math.random() * STD.NUM_DANCERS)]);
    }
    if(Math.random() < 0.05) {
      upright_dancer(STD.dancers[Math.floor(Math.random() * STD.NUM_DANCERS)]);
    }
	if(STD.ready) {
	  if(STD.input.action) {
		STD.input.action = false;
        STD.ready = false;
        STD.modeend = time + 100;
	  }
	} else {
	  if(STD.modeend && time > STD.modeend) {
        STD.sfx.gong.currentTime = 0;
        STD.sfx.gong.play();
        //navigator.vibrate(100);
		STD.mode = "select";
        STD.menu = null;
		STD.modestart = time;
		STD.modeend = 0;
        STD.ui.action.classList.toggle("hidden", true);
        STD.ui.left.classList.toggle("hidden", false);
        STD.ui.right.classList.toggle("hidden", false);
        for(var i = 0; i < STD.NUM_DANCERS; ++i) {
          STD.dancers[i].speedx = STD.dancers[i].speedy = 0;
          STD.dancers[i].y = 48;
          STD.dancers[i].x = (16) + (i * 32);
        }
        STD.menu = null;
		STD.ready = true;
	  }
	}
	break;
  case "select":
    if(STD.ready) {
      if(STD.input.left) {
        STD.input.left = false;
        if(STD.menu === null) {
          STD.menu = Math.floor((time / 100)) % (STD.NUM_DANCERS + 1);
        }
//        STD.menu = STD.menu || 0;
        --STD.menu;
//        STD.modeend = time + 1000;
        STD.modestart = time - 2000;
      }
      if(STD.input.right) {
        STD.input.right = false;
        if(STD.menu === null) {
          STD.menu = Math.floor((time / 100)) % (STD.NUM_DANCERS + 1);
        }
//        STD.menu = STD.menu || 0;
        ++STD.menu;
//        STD.modeend = time + 1000;
        STD.modestart = time - 2000;
      }
      if(STD.menu !== null) {
        if(STD.menu < 0) STD.menu = STD.NUM_DANCERS;
        if(STD.menu > STD.NUM_DANCERS) STD.menu = 0;
      }
      if((time - STD.modestart) > 3000) {
        STD.ready = false;
      }
    } else {
      if(STD.menu === null) {
        STD.menu = Math.floor(Math.random() * (STD.NUM_DANCERS + 1));
      }
      if(!STD.modeend) {
        STD.modeend = time + 2000;
      }
	  if(STD.modeend && time > STD.modeend) {
		STD.mode = "play";
		STD.modestart = time;
		STD.modeend = 0;
        STD.control = [];
        if(STD.menu === STD.NUM_DANCERS) {
          for(i = 0; i < STD.NUM_DANCERS; ++i) {
            STD.control.push(i);
          }
        } else {
          STD.control.push(STD.menu);
          for(i = 0; i < STD.NUM_DANCERS; ++i) {
            if(STD.menu !== i) {
              STD.dancers[i].x = STD.dancers[i].y = STD.dancers[i].speedx = STD.dancers[i].speedy = 0;
            }
          }
        }
        STD.flames = [];
        for(var i = 0; i < STD.NUM_DANCERS; ++i) {
          STD.dancers[i].speedx = (Math.random() * STD.MAX_SPEED) - STD.MAX_SPEED;
          STD.dancers[i].speedy = (Math.random() * STD.MAX_SPEED) - STD.MAX_SPEED;
        }
        STD.progress = 0;
        STD.wind.speed = STD.wind.influence = 0;
        STD.wind.fx = [];
        STD.fall = [];
        start_song(STD.menu);
		STD.ready = true;
	  }
    }
    break;
  case "play":
    // on the wind
    if(STD.music && STD.music.currentTime > 6.8) {
      if(!STD.wind.influence) {
        STD.wind.influence = 100 * (Math.random() < 0.5 ? 1 : -1);
      }
      STD.wind.speed += STD.wind.influence / 100;
      STD.wind.influence += Math.random() - 0.5;
      STD.wind.influence = Math.min(STD.wind.influence, STD.MAX_WIND);
      STD.wind.influence = Math.max(STD.wind.influence, 0 - STD.MAX_WIND);

      if(STD.wind.fx.length < 10) {
        STD.wind.fx.push({
          x: Math.random() * STD.W,
          y: Math.random() * STD.H,
          shape: Math.random() < 0.5 ? 0 : 1
        });
      }
      STD.wind.fx.forEach(function(fx) {
        fx.x += STD.wind.speed;
        if(fx.x < 0) {
          fx.x = STD.W;
        } else if(fx.x > STD.W) {
          fx.x = 0;
        }
        fx.y += 1 - (Math.random() * 2);
      });
      /*
      STD.wind.fx = STD.wind.fx.filter(function(fx) {
        if(STD.music.currentTime > 13) {
          //TODO burn leaf
          if(fx.y < STD.H) {
            return true;
          }
          return false;
          for(i = 0; i < 20; ++i) {
            STD.flames.push({
              x: fx.x - 8 + Math.random() * 16,
              y: fx.y - 8 + Math.random() * 16,
              speed: Math.random() * -1.25,
              shape: Math.floor(Math.random() * 6),
              scale: 1 + (Math.random() * 2)
            });
          }
        }
        return false;
      });
      */
    }

    // see them fall
    if(STD.music && STD.music.currentTime > 10) {
      var maxfall = derp(1, 5,
                         (STD.music.currentTime - 10) /
                         (STD.music.duration - 10));
      if(STD.fall.length < maxfall) {
        STD.fall.push({
          x: Math.random() * STD.W,
          y: -8,
          speedx: (Math.random() * 2) - 1,
          speedy: 0.2,
          shape: Math.floor(Math.random() * 4),
//          scale: Math.random() + 1,
          flip: Math.random() < 0.5 ? 0 : 1,
          bonked: false
        });
      }
      STD.fall.forEach(function(fall) {
        gravity(fall);

        fall.x += fall.speedx;
        fall.y += fall.speedy;
      });
      STD.fall = STD.fall.filter(function(fall) {
        return fall.y < STD.H;
      });
      bonk_fallers();
    }

    // on the fire
    if(STD.music && STD.music.currentTime > 13) {
      STD.flames.push({
        x: Math.random() * STD.W,
        y: STD.H + 8,
        speed: Math.random() * -1,
        shape: Math.floor(Math.random() * 6),
        scale: 2
      }, {
        x: Math.random() * STD.W,
        y: STD.H + 8,
        speed: Math.random() * -1,
        shape: Math.floor(Math.random() * 6),
        scale: 1.5
      });
      STD.flames.forEach(function(flame) {
        flame.y += flame.speed;
        flame.scale -= 0.025;
        flame.x += 1 - (Math.random() * 2);
      });
      STD.flames = STD.flames.filter(function(flame) {
        return flame.scale > 0;
      });
    }

    if(STD.musicended) {
      STD.musicended = false;
      STD.ready = false;
      STD.modeend = time;
      if(STD.control.length) {
        ++STD.survived;
        STD.progress = 1;
      }
    }
	if(STD.ready) {
      STD.dancers.forEach(function(dancer) {
        if(STD.control.indexOf(dancer.who) >= 0) {
          // we control this dancer
          //windy
          if(STD.music && STD.music.currentTime > 6.8) {
            dancer.x += (STD.wind.speed / 2);
          }
          move_dancer(dancer);
          if(dancer.y === STD.H && STD.music && STD.music.currentTime > 13) {
            STD.survived = 0;
            //STD.progress = STD.music.currentTime / STD.music.duration;
            STD.progress = STD.music.currentTime / 21.8335; //stupid apple
            // die burning
            STD.sfx.fire.currentTime = 0;
            STD.sfx.fire.play();
            for(i = 0; i < 100; ++i) {
              STD.flames.push({
                x: dancer.x - 8 + Math.random() * 16,
                y: dancer.y - 8 + Math.random() * 16,
                speed: Math.random() * -1.25,
                shape: Math.floor(Math.random() * 6),
                scale: 1 + (Math.random() * 2)
              });
            }
            STD.control = STD.control.filter(function(ctl) {
              return ctl !== dancer.who;
            });
          }
	      if(STD.input.left) {
            upleft_dancer(dancer);
	      }
	      if(STD.input.right) {
            upright_dancer(dancer);
	      }
        }
      });
	  STD.input.left = false;
	  STD.input.right = false;
	} else {
	  if(STD.modeend && time > STD.modeend) {
		STD.mode = "finish";
		STD.modestart = time;
		STD.modeend = 0;
        // subtly change action button after first round
        var bctx = STD.ui.action.getContext("2d");
	    bctx.drawImage(STD.gpx, 192, 192, 64, 32,0, 0, 256, 128);

        STD.ui.action.classList.toggle("hidden", false);
        STD.ui.left.classList.toggle("hidden", true);
        STD.ui.right.classList.toggle("hidden", true);
		STD.ready = true;
	  } else {
		if(time < STD.modeend - 1500) {
		}
	  }
	}
    break;
  case "finish":
    STD.dancers.forEach(move_dancer);
    if(Math.random() < 0.05) {
      upleft_dancer(STD.dancers[Math.floor(Math.random() * STD.NUM_DANCERS)]);
    }
    if(Math.random() < 0.05) {
      upright_dancer(STD.dancers[Math.floor(Math.random() * STD.NUM_DANCERS)]);
    }
    if(STD.ready) {
      if(STD.input.action) {
        if(time < STD.modestart + 2000) {
          // ignore input first 2s
          STD.input.action = false;
          break;
        }
        STD.input.action = false;
        STD.modeend = time + 500;
        STD.ready = false;
      }
    } else {
      if(time > STD.modeend) {
        init();
        STD.modestart = time;
      }
    }
    break;
  }
}

function render(time) {
  window.requestAnimationFrame(render);

  // call tick() at desired framerate regardless of draw framerate
  if(STD.frametime) {
	STD.frametime += time - STD.lastframe;
  } else {
	STD.frametime = time - STD.lastframe;
  }
  while(STD.frametime >= STD.tickwait) {
	tick(time);
	STD.tick++;
	STD.frametime -= STD.tickwait;
	STD.frametime /= 1.5;  // HACK: prefer natural to exact fps
  }

  switch(STD.mode) {
  default:
	render_title(time);
	break;
  case "select":
	render_select(time);
	break;
  case "play":
	render_play(time);
	break;
  case "finish":
	render_finish(time);
	break;
  }

  STD.lastframe = time;
}

function mousemove(e) {
	var x = e.clientX;
	var y = e.clientY;
	var elm = e.target
	while(elm && elm.offsetLeft !== undefined) {
		x -= elm.offsetLeft;
		y -= elm.offsetTop;
		elm = elm.offsetParent;
	}
	STD.input.pointer.x = Math.floor(x / STD.canvas.clientWidth * STD.W);
	STD.input.pointer.y = Math.floor(y / STD.canvas.clientHeight * STD.H);
}
function mousedown(e) {
	STD.input.pointer.click = true;
}
function mouseup(e) {
	STD.input.pointer.click = false;
}
function touchmove(e) {
	var touches = e.changedTouches;
	if(e.changedTouches && e.changedTouches.length) {
		e = e.changedTouches[0];
	}
	mousemove(e);
	e.preventDefault();
}

function pause() {
  if(STD.music) {
    STD.music.pause();
  }
}
function resume() {
  if(STD.music) {
    STD.music.play();
  }
}

window.addEventListener("load", function() {
  STD.canvas = document.querySelector("canvas");
  STD.ctx = STD.canvas.getContext("2d");
  STD.ctx.imageSmoothingEnabled = false;
  STD.ctx.mozImageSmoothingEnabled = false;
  STD.ctx.webkitImageSmoothingEnabled = false;
  STD.scale = STD.canvas.width / STD.W;
  STD.gpx = document.querySelector("img#gpx");

  var stroph = localGet("trophies");
  try {
	//console.log(stroph);
	stroph = JSON.parse(stroph);
  } catch(ignore) {
	alert("corrupt trophy data ignored.");
	stroph = {};
  }
  STD.trophies = stroph || {};

  STD.sfx.gong = document.querySelector("audio#gong");
  STD.sfx.fire = document.querySelector("audio#fire");
  STD.sfx.bonk = document.querySelector("audio#bonk");

  init();

  STD.ui.action = document.querySelector("canvas#start");
  STD.ui.left = document.querySelector("canvas#left");
  STD.ui.right = document.querySelector("canvas#right");

  //  for(var btn in STD.ui) {
  Object.keys(STD.ui).forEach(function(btn) {
    var ctx = STD.ui[btn].getContext("2d");
    ctx.imageSmoothingEnabled = false;
    ctx.mozImageSmoothingEnabled = false;
    ctx.webkitImageSmoothingEnabled = false;

    if(btn === "right") {
      ctx.scale(-1, 1);
    }
	ctx.drawImage(STD.gpx, 128 + (btn === "action" ? 64 : 0), 160, 64, 32,
				  0, 0, 256 * (btn === "right" ? -1 : 1), 128);
/*
    ctx.canvas.addEventListener("mousedown", function(e) {
      STD.input[btn] = true;
    });
    ctx.canvas.addEventListener("mouseup", function(e) {
      STD.input[btn] = false;
    });
    ctx.canvas.addEventListener("touchstart", function(e) {
      STD.input[btn] = true;
    });
    ctx.canvas.addEventListener("touchend", function(e) {
      STD.input[btn] = false;
    });
*/
    ctx.canvas.addEventListener("click", function(e) {
      if(!STD.touch) {
        STD.input[btn] = true;
      }
      if(STD.music) { //stupid apple
        STD.music.play();
      }
//      STD.canvas.focus();
    });
/*
*/
    ctx.canvas.addEventListener("touchstart", function(e) {
      if(!STD.music) {
        e.preventDefault();
      }
      STD.input[btn] = true;
      ++STD.touch;
    });
  });
  document.addEventListener("visibilitychange", function() {
    if(document.hidden) {
      pause();
    } else {
      resume();
    }
  });

  STD.canvas.addEventListener("mousemove", mousemove);
  //	STD.canvas.addEventListener("touchstart", touchmove);
  //	STD.canvas.addEventListener("touchmove", touchmove);
  STD.canvas.addEventListener("mousedown", mousedown);
  STD.canvas.addEventListener("mouseup", mouseup);
  STD.canvas.addEventListener("mouseleave", mouseup);
  STD.canvas.addEventListener("touchstart", function(e) {
	var touches = e.changedTouches;
	if(e.changedTouches && e.changedTouches.length) {
	  e = e.changedTouches[0];
	}
	//mousemove(e);
	mousedown(e);
  });

  window.requestAnimationFrame(render);
});

function handlekey(code, press) {
	if(!STD.ready && press) {
		return;
	}
	switch(code) {
	case 38:  //up
	case 104: //num8
		//  case 75:  //k
	case 87:  //w
		// case 67:  //c
//		STD.input.up = press;
		break;
	case 40:  //down
	case 98:  //num2
		//  case 74:  //j
	case 83:  //s
		// case 68:  //d
//		STD.input.down = press;
		break;
	case 37:  //left
	case 100: //num4
		//  case 72:  //h
	case 65:  //a
		// case 69:  //e
		STD.input.left = press;
		break;
	case 39:  //right
	case 102: //num6
		//  case 76:  //l
	case 68:  //d
		// case 70:  //f
		STD.input.right = press;
		break;
	case 32:  //space
	case 13:  //enter
		// case 74:  //j
		STD.input.action = press;
		break;
	default:
		 //   console.log(e.keyCode);
		break;
	}
}
window.addEventListener("keydown", function(e) {
	handlekey(e.keyCode, true);
});
window.addEventListener("keyup", function(e) {
	handlekey(e.keyCode, false);
});
